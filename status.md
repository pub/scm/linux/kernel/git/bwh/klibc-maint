# Test status

## Test cases

test-many-klibcs implements:

1. Build for each architecture (with a cross-compiler where needed)
2. Run various programs (using qemu-user where needed) in static and
   shared configurations:
   a. Many self-test programs (through system shell)
   b. "sh -c exit"
   c. "sh -c '.../bin/true; exit'"

## Test failures

### 2.0.10

With patched QEMU 3.1:

* arm, i386, m68k, parisc, ppc, sh:
  * Fail to build (with Linux 5.19 headers)
* arm64:
  * 2a:malloctest2, 2a:setjmptest, 2a:sigint: Fail in shared build
    with GNU toolchain (gcc 12.1.0, binutils 2.38.90.20220713)
  * 2c: Fails with SIGSEGV in static build with LLVM, but only on QEMU
* riscv64:
  * 2a:sigint: Fails when built with Clang, but only on QEMU
* s390x:
  * 2a:environ, 2a:opentest, 2a:setjmptest: Produce wrong output when built
    with Clang, but only on QEMU
  * 2a:malloctest, 2a:malloctest2, 2c: Appear to loop forever when built
    with Clang, but only on QEMU
  * 2a:sscanf: Exits with error when built with Clang, but only on QEMU

### 2.0.11, 2.0.12

With patched QEMU 3.1:

* mipsisa64r6, mipsisa64r6el:
  * Linker error "relocation truncated to fit: R_MIPS_PC26_S2" when
    building a shared executable with gcc
* riscv64:
  * 2a:sigint: Fails when built with Clang, maybe only on QEMU
* s390x:
  * 2a:environ, 2a:opentest, 2a:setjmptest: Produce wrong output when built
    with Clang, maybe only on QEMU
  * 2a:malloctest, 2a:malloctest2, 2c: Appear to loop forever when built
    with Clang, maybe only on QEMU
  * 2a:sscanf: Exits with error when built with Clang, maybe only on QEMU
* sh4:
  * All executables hang when built with gcc 12

With QEMU 7.2:

* armhf:
  * All tests fail (SIGSEGV) in shared build. Seems to be a QEMU bug.
* mipsisa64r6, mipsisa64r6el:
  * Linker error "relocation truncated to fit: R_MIPS_PC26_S2" when
    building a shared executable with gcc
* riscv64:
  * All tests fail in shared build. Seems to be a QEMU bug.
  * 2a:sigint: Fails when built with Clang
* sh4:
  * All executables hang when built with gcc 12

### Known bugs elsewhere

QEMU 3.1 had a bug in handling of struct timveval on sparc64, and a
bug in the ELF loader.  Additionally, it has a lot of bugs in signal
handling on alpha and sparc.  All of those are fixed in my local
build, qemu-user-static_3.1+dfsg-2.1_amd64.deb.

QEMU 6.1 fixes those plus the s390x failures, but also has regressions
for armhf, hppa, and riscv64.

QEMU 7.2 fixes hppa, but still has regressions for armhf and riscv64.
But we need it for time64 support.

Clang 13 has a bug that affects i386, -mregparm, and optimisation of
printf() to puts().  The call to puts() passes arguments using the
usual stack-based calling convention.  This is now worked-around
by disabling CONFIG_REGPARM.

LLD 13 has a bug affecting mips and mips64 that puts trap instructions
(sigrie) instead of nops in branch delay slots.  Patching these back
to nops made the tests pass.  I have only seen this in klibc.so, not
in any executables.

LLD 13 seems to have a bug in PowerPC global register initialisation
affecting most tests on ppc64.

LLD 13 is missing required features for riscv64 and sparc64.
